package main

import (
	"net/http"
	"net/http/cookiejar"
)

var (
	CurrentSession *Session
)

func main() {
	cookieJar, _ := cookiejar.New(nil)
	CurrentSession = &Session{
		CookieJar: cookieJar,
		Client: &http.Client{
			Jar: cookieJar,
		},
	}
}
