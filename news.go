package main

import (
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"strings"
)

const (
	NewsChannelURL = "https://www.edwise.se/Secure/Rexnet/Information/View/InformationContainer.aspx"
)

type NewsArticle struct {
	Content string
	Publication struct {
		Name string
		Date string
	}
}

func (s *Session) GetNews() map[int]*NewsArticle {
	Articles := make(map[int]*NewsArticle)
	res, _ := s.SendGetReq(NewsChannelURL)
	doc, _ := goquery.NewDocumentFromReader(strings.NewReader(res))
	doc.Find(".clearfix").Each(func(i int, s *goquery.Selection) {
		content := strings.TrimSpace(s.Find(".content").Text())
		if content != "" {
			Articles[i] = &NewsArticle{
				Content: content,
				Publication: struct {
					Name string
					Date string
				}{
					Name: s.Find(fmt.Sprintf("#NewsItemList_NewsItemRepeater_ctl%02d_PublishedbyinServiceChannel", i)).Text(),
					Date: s.Find(".publication-date").Text(),
				},
			}
		}
	})
	return Articles
}
