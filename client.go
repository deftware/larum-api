package main

import (
	"io/ioutil"
	"log"
	"net/http"
	"net/http/cookiejar"
	"net/url"
	"regexp"
	"strings"
)

const (
	SetupURL = "https://www.edwise.se/Secure/Default.aspx?idpmethod=saml&domain=Umea&Actor=Actor_Client"
	AuthURL = "https://prod-liam.service.tieto.com/saml/authenticate/SKOLFED-IDP-UMEAEDU-CLIENT"
	ProdURL = "https://prod-liam.service.tieto.com:443/saml/authenticate/BROKERSP-UMEAEDU-CLIENT"
	LoginPage = "https://efs.umea.se/adfs/ls/"
	RedirectAuthURL = "https://services.edwise.se/Login/redirectAuth.aspx"
)

type Session struct {
	Username string
	CookieJar *cookiejar.Jar
	Client *http.Client
	HomepageData string
}

func (s *Session) SendPostReq(url string, data url.Values) (string, error) {
	res, err := s.Client.PostForm(url, data)
	if err != nil {
		return "", err
	}
	body, err := ioutil.ReadAll(res.Body)
	_ = res.Body.Close()
	return string(body), nil
}

func (s *Session) SendGetReq(url string) (string, error) {
	res, err := s.Client.Get(url)
	if err != nil {
		return "", err
	}
	data, err := ioutil.ReadAll(res.Body)
	_ = res.Body.Close()
	return string(data), nil
}

func (s *Session) Login(username, password string) {
	r := regexp.MustCompile(`value=\"(.*?)\"`)
	// 1st phase
	log.Printf("Initializing connection to %s\n", SetupURL)
	res, _ := s.SendGetReq(SetupURL)
	// 2nd phase
	log.Printf("Setting up %s\n", AuthURL)
	matches := r.FindAllStringSubmatch(res, -1)
	res, _ = s.SendPostReq(AuthURL, url.Values{
		"SAMLRequest": {matches[0][1]},
		"RelayState": {matches[1][1]},
	})
	// 3rd phase
	log.Printf("Redirecting to sign in page %s\n", LoginPage)
	matches = r.FindAllStringSubmatch(res, -1)
	res, _ = s.SendPostReq(LoginPage, url.Values{
		"SAMLRequest": {matches[0][1]},
	})
	// Sign in
	r = regexp.MustCompile(`action="\/adfs\/ls\/\?client-request-id=(.*?)\"`)
	matches = r.FindAllStringSubmatch(res, -1)
	authUrl := LoginPage + "?client-request-id=" + matches[0][1]
	log.Printf("Signing in at %s\n", authUrl)
	res, _ = s.SendPostReq(authUrl, url.Values{
		"UserName": {"elevad\\" + username},
		"Password": {password},
		"Kmsi": {"true"},
		"AuthMethod": {"FormsAuthentication"},
	})
	// 4th phase
	log.Printf("Signed in, redirecting to %s\n", ProdURL)
	r = regexp.MustCompile(`value=\"(.*?)\"`)
	matches = r.FindAllStringSubmatch(res, -1)
	res, _ = s.SendPostReq(ProdURL, url.Values{
		"SAMLResponse": {matches[0][1]},
	})
	// 5th phase
	log.Printf("Redirecting to lärum %s\n", RedirectAuthURL)
	matches = r.FindAllStringSubmatch(res, -1)
	res, _ = s.SendPostReq(RedirectAuthURL, url.Values{
		"SAMLResponse": {matches[0][1]},
		"RelayState": {matches[1][1]},
	})
	// Connected :)
	log.Println("Connected to lärum :)")
	s.HomepageData = res
	r = regexp.MustCompile(`return\sfalse\;\"\>(.*?)\<`)
	matches = r.FindAllStringSubmatch(res, -1)
	for _, v := range matches {
		if (len(strings.TrimSpace(v[1])) != 0) {
			username = v[1]
		}
	}
	username = strings.Replace(username, "&nbsp;", " ", 1)
	s.Username = username
	log.Printf("Logged in as %s\n", username)
}
